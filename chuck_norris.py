'''
Dieses Modul holt sich einen Witz von api.chucknorris.io
'''
import requests
import json

def getjoke():
    '''
    Funktion holt per API Call einen beliebigen Chuck Norris Witz ab
    '''
    url = "https://api.chucknorris.io/jokes/random"

    # Vom Modul requests wird die Funktion request aufgerufen, ein HTTP GET gegen die definierte URL
    response = requests.request("GET", url)

    # Umwandlung des JSON-Strings in ein Python Dicitionary
    response_json=json.loads(response.text)

    #Rückgabe eines Elements des Dictionarys -> dem Witz
    return(response_json['value'])