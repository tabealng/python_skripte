'''
Dieses Modul sendet eine Nachricht in WebEx an die gegebene E-Mail-Adresse
'''
import requests
import json
from chuck_norris import getjoke

def send_message(text, email):
    '''gegebener text wird an die email via REST API Call an WebEx versendet'''
    url = "https://webexapis.com/v1/messages"
    payload = json.dumps({
      "toPersonEmail": email,
      "text": text
    })
    # hinter Bearer den Token aus developer.webex.com kopieren
    headers = {
      'Authorization': 'Bearer XXX',
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    print(response.text)

#Aufrufen der Funktion und Übergabe eines Chuck-Norris-Witz
send_message(getjoke(), "test@example.com")