'''Modul zur Berechnung einer Kreisfläche'''

def kreisflaeche(radius):
    ''' Eingabe des Radius, Funktion printet Ergebnis der Kreisfläche in Form eines Satzes'''
    PI=3.14159
    flaeche=radius*radius*PI
    print("Die Fläche eines Kreises mit dem Radius",radius, "ist", flaeche)

kreisflaeche(int(input('Geben sie einen Radius ein:')))
